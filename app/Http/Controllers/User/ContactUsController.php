<?php

namespace App\Http\Controllers\User;

use App\Model\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactUsController extends Controller
{
    public function index()
    {
        return view('user.contactUs');
    }
    public function store(Request $request)
    {
        $message = new Contact();

        $message->first_name = $request->first_name;
        $message->last_name = $request->last_name;
        $message->email = $request->email;
        $message->comment = $request->comment;

        $message->save();
        
        return redirect()->back();
    }
}
